#include <iostream>
#include <string>
#include <vector>
using namespace std;
typedef vector<string> Wektor;

///Funkcja sprawdzajaca czy dany wyraz jest palindromem
bool jestPal(string wyraz) {
	int koniec = wyraz.size()-1;
	if (koniec<=0) {
		return true;
	} else {
		return wyraz[0] == wyraz[koniec] && jestPal(wyraz.substr(1, koniec-1));
	} 
}

void ZnajdzPermutacje(string wyraz, Wektor &lista, string permutacja){ 	//PERMUTACJE, ZNAJDOWANIE PERMUTACJI I SPRAWDZANIE CZY JEST PALINDROMEM, DODANIE DO TABLICY
	int dlugosc=wyraz.size()-1;
	int i=0;
	if(wyraz.empty()){
  		if(jestPal(permutacja))											//sprawdzenie czy podany wyraz jest palindromem
  			lista.push_back(permutacja);								//jesli tak, dodanie do wektora palindromow
  	}
   	
	for(int i=0; i<wyraz.size(); ++i){								
   		string tmp=wyraz;
		tmp.erase(i,1);
		string perm=permutacja;
		perm+=wyraz.at(i);
		ZnajdzPermutacje(tmp,lista,perm);											
	}
}
//Funkcja usuwajaca duplikaty palindromow z wektora
int usunDup(Wektor &palList){
	for(int i=0; i<palList.size(); i++){		//petla sprawdzajaca czy juz koniec wektora, porownujaca dwa stringi
		for(int j=1; j<palList.size(); j++)
			if(palList[i]==palList[j])
				palList.erase(palList.begin()+j);			//jesli dwa stringi sa takie same, sa one usuwane z wektora
	}
	return 0;
}


//Glowna funkcja
int main(){
	string wyraz, permutacja;							//WPROWADZENIE LANCUCHA ZNAKOW DO PROGRAMU
	cout<<"Wprowadz wyraz: ";
	cin>>wyraz;
	cout<<endl;

	Wektor ListaPal;						//WEKTOR PALINDROMOW UTWORZONYCH Z LANCUCHA ZNAKOW PODANEGO NA POCZATKU
	
	ZnajdzPermutacje(wyraz, ListaPal, permutacja); //znajdowanie permutacji
	
	if(!ListaPal.empty()){							//jesli wektor jest niepusty, wyswietlana jest lista palindromow
													//otrzymanych z kombinacji poczatkowego lancucha znakow
		cout<<endl<<endl<<"Lista otrzymanych palindromow: "<<endl;
	for(int i=0; i<ListaPal.size(); i++)
		cout<<ListaPal[i]<<endl;	
	}
	
	else{
		cout<<"Blad, z tego zestawu znakow nie da sie utworzyc palindromu"<<endl;
	}
	cout<<endl<<"Czy chcesz usunac dupikaty? Jesli tak, wcisnij 1, jesli nie, wcisnij 0: ";
	int opcja;
	cin>>opcja;
	cout<<endl;
	if(opcja==1){											//Sprawdzenie czy uzytkownik chce usunac duplikaty
		usunDup(ListaPal);									//jesli tak, duplikaty sa usuniete
		cout<<"USUNIETO";
		cout<<endl<<"Lista otrzymanych palindromow po usunieciu: "<<endl;
	for(int i=0; i<ListaPal.size(); i++)
		cout<<ListaPal[i]<<endl;							//wyswietlenie wszystkich palindromow po usunieciu duplikatow
	}	
	
	ListaPal.clear();										//oproznienie wektora
	return 0;
}
