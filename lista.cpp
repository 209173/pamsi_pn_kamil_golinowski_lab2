#include <iostream>

using namespace std;


//klasa obejmujaca pojecie wezla
template <class typ>
class Wezel{
	public:
		typ dane;							//zmienna przechowujaca dane
		class Wezel *nastepny;				//zmienna wskaznikowa, wskazuje na nastepny wezel
		Wezel(typ nowy, Wezel *inny=0){		//konstruktor parametryczny	
			dane=nowy;
			nastepny=inny;			
		}
};


//Klasa obslugujaca pojecie listy
template <class typ>
class Lista{
	public:
		Lista(){							//konstruktor bezparametryczny
			glowa=ogon=0;
		}	
		
		~Lista(){							//destruktor
			for(Wezel<typ> *i; !pusty(); ){		//deklarujemy pomocniczy wezel i dopoki lista jest niepusta, wykonujemy instrukcje:
				i=glowa->nastepny;			//przypisanie do zmiennej tymczasowej wskaznika na glowe listy
				delete glowa;				//usuniecie glowy
				glowa=i;					//przesuniecie wskaznika na nowa glowe
			}
		}

		bool pusty(){						//funkcja sprawdzajaca czy lista jest pusta
			if(glowa==0)
				return true;
			else
				return false;
		}

		void dodaj_glowe(typ wpis){			//funkcjia dodajaca element do listy w miejsce glowy
			glowa=new Wezel<typ>(wpis, glowa);	//alokacja nowej glowy z nowym wpisem i z wskaznikiem na stara glowe 
			if(ogon==0)						//jesli nie ma w liscie ogona,
				ogon=glowa;					//glowa staje sie jednoczesnie ogonem
		}
		
		void dodaj_ogon(typ wpis){			//funkcja dodajaca element do listy w miejsce ogona
			if(ogon!=0){
				ogon->nastepny=new Wezel<typ> (wpis);
				ogon=ogon->nastepny;
			}
			else
				glowa=ogon=new Wezel<typ>(wpis);
		}
		
		typ usun_glowe() {
			typ dana=glowa->dane;			//pomocnicza zmienna przechowujaca dany element, ktory przechowywala glowa
			Wezel<typ> *i=glowa;					//pomocniczy wezel przechowujacy cala glowe
			if(glowa==ogon){				//jesli glowa jest jednoczesnie ogonem,
				glowa=0;					//to obie wartosci zostaja przyrownane do 0
				ogon=0;
			}
			else							//w przeciwnym wypadku glowa zostaje przepisana
				glowa=glowa->nastepny;		//do nastepnego elementu
			delete i;						//i zostaje usunieta
			return dana;					//funkcja zwraca dana, ktora przechowywala glowa	
		}
		
		typ usun_ogon() {
			typ dana=ogon->dane;			//pomocnicza zmienna przechowujaca dany element, ktory przechowywal ogon
			if(ogon==glowa){				//jesli ogon jest jednoczesnie glowa,
				delete glowa;				//to usuwamy glowe
				glowa=0;					//i obie wartosci zostaja przyrownane do 0
				ogon=0;
			}
			else{							//w przeciwnym wypadku szukamy elementu poprzedzajacego ogon
				Wezel<typ> *i=glowa;				//tworzymy tymczasowy wezel i przypisujemy mu wartosc glowy
				while(i->nastepny!=ogon){	//szukamy przedostatniego wezla i przypisujemy go do zmiennej tymczasowej
				i=i->nastepny;
				}			
				delete ogon;				//usuwamy ogon
				ogon=i;						//przedostatni wezel staje sie ogonem
				ogon->nastepny=0;			//wezel ostatni nie wskazuje na nic, dlatego zerujemy wskaznik
			}
			
			return dana;					//funkcja zwraca dana, ktora przechowywala glowa
		}
		
		void usun_cala(){ 
		Wezel<typ> *i;
			while(glowa!=0){
					i=glowa;
					glowa=glowa->nastepny;
					delete i;
			};
		}
		
		void wyswietl(){
			for(Wezel<typ> *i=glowa; i!=0; i=i->nastepny){
				cout<<i->dane<<" ";
			}
		}
		
		void menu(){
			int opcja;
			typ nowa;
		do{
cout << "1.Dodaj wartosc na poczatek listy"<<endl;
cout << "2.Dodaj wartosc na koniec listy"<<endl;
cout << "3.Usun wartosc z poczatku listy"<<endl;
cout << "4.Usun wartosc z konca listy"<<endl;
cout << "5.Usun cala liste"<<endl;
cout << "6.Wyswietl liste"<<endl;
cout << "0.Koniec"<<endl;
cout << "Wybierz opcje";
cin >> opcja;
switch(opcja)
{
 case 0: break; 
 case 1: {
 		cout<<"Podaj wartosc ktora chcesz dodac: ";
 		cin>>nowa;
 		 this->dodaj_glowe(nowa);
 		 break;
 	   	 }
 case 2: {
 		cout<<"Podaj wartosc ktora chcesz dodac: ";
 		cin>>nowa;	
 		 this->dodaj_ogon(nowa);
 		 break;
 		 }
 case 3:{
 		if(!this->pusty())
 			cout<<"Zostala usunieta liczba: "<<this->usun_glowe()<<endl;
		else
			cout<<"Blad, lista jest pusta!"<<endl;
		break;
		 }		 
 case 4:{
 	    if(!this->pusty())
 			cout<<"Zostala usunieta liczba: "<<this->usun_ogon()<<endl;
		else
			cout<<"Blad, lista jest pusta!"<<endl;
		break;
 }		
 case 5:{
 		if(!this->pusty()){
		 	this->usun_cala();
 			cout<<"Lista zostala usunieta"<<endl;
 		}
		else
			cout<<"Blad, lista jest pusta!"<<endl;
	break;
 }
 case 6:{
 		if(!this->pusty())
 			this->wyswietl();
		else
			cout<<"Brak elementow na liscie!"<<endl;
	break;
 }
}
cout<<endl<<endl<<endl;
} while(opcja!=0);	
		}
		
	private:
		Wezel<typ> *glowa, *ogon;
};





int main(){
		int opcja;
		cout<<"Wybierz typ danych na ktorych chcesz operowac:"<<endl;
		cout<<"1. Liczba calkowita int"<<endl;
		cout<<"2. Typ znakowy char"<<endl;		
		cout<<"3. Liczba zmiennoprzecinkowa float"<<endl;
		cout<<"4. Lancuch znakow string"<<endl;
		cin >> opcja;
		if(opcja==1){
		Lista<int> przyklad;
		cout<<endl;
		przyklad.menu();
		}
		if(opcja==2){
			Lista<char> przyklad;
		cout<<endl;
		przyklad.menu();
		}
		if(opcja==3){
			Lista<float> przyklad;
			cout<<endl;
		przyklad.menu();
		}
		if(opcja==4){
			Lista <string> przyklad;
			cout<<endl;
		przyklad.menu();
		}
		else
			cout<<"Bledny parametr!"<<endl;


	return 0;
}
