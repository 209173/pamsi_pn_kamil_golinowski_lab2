#include <iostream>
#define ROZMIAR 100
using namespace std;


//klasa realizujaca pojecie kolejki jako okrag o zadanej wczesniej dlugosci ROZMIAR
template <class typ>
class Kolejka{
	public:
		Kolejka(){							//konstruktor bezparametryczny
			pierwszy=ostatni=-1;			
		}
		
		bool pelna() const {				//kolejka jest pelna jesli element pierwszy jest na 1 a element ostatni jest na ROZMIAR miejscu tablicy 
											//lub ostatni jest tuz za pierwszym
			return (pierwszy==0 && ostatni==ROZMIAR-1) || pierwszy==ostatni+1;		
		}
		
		bool pusta() const {
			return pierwszy==-1 && ostatni==-1;
		}
		
		void Dodaj(typ wpis){							//funkcja dodajaca element do kolejki
			if(!pelna()){								//mozna dodac element tylko jesli kolejka jest niepelna
				if(ostatni==ROZMIAR-1||ostatni==-1)	{	//jesli ostatni element jest juz na ROZMIAR miejscu, lub kolejka jest pusta
					zawartosc[0]=wpis;					//zapis nastepuje na pierwszym miejscu w kolejce
					ostatni=0;							//wtedy dany element jest zarowno ostatni
					if(pierwszy==-1)
						pierwszy=0;						//jak i pierwszy
				}
				else{
					ostatni++;
					zawartosc[ostatni]=wpis;			//w przeciwnym wypadku element dodany jest jako nastepny w kolejce
				}
			}	
			
			else 										//jesli kolejka jest pelna, wyswietli sie odpowiedni komunikat
				cout<<"Kolejka jest pelna!"<<endl;
		}
		
		typ Usun(){										//funkcja usuwajaca pierwszy element z kolejki
			typ i=zawartosc[pierwszy];					//zmienna tymczasowa, przypisanie do pierwszego elementu kolejki
			if(pierwszy==ostatni)						//jesli pierwszy i ostatni element to to samo
				pierwszy=ostatni=-1;					//wtedy kolejka konczy sie i zmienne wracaja do wartosci poczatkowych
			else 									
				if(pierwszy==ROZMIAR-1)					//jesli pierwszy element jest na ROZMIAR miejscu, licznik sie "przekreca"
					pierwszy=0;							//i pierwszym elementem jest element na miejscu 1 w kolejce
				else
					pierwszy++;							//jesli nastapila inna sytuacja, nastepuje przesuniecie kolejki
			return i;									//funkcja zwraca wartosc usunietego elementu
		}
		
		void Wyswietl() const {								
			if(pierwszy<=ostatni)							//jesli element pierwszy jest blizej poczatku tablicy niz element ostatni
				for(int i=pierwszy; i<ostatni+1; i++){		//wtedy wykonuje sie zwykla petla od pierwszego do ostatniego
					cout<<zawartosc[i]<<" ";				//wypisz zawartosc
				}
			else{
				for(int i=pierwszy; i<ROZMIAR; i++){		//jesli natomiast pierwszy jest dalej niz ostatni
					cout<<zawartosc[i]<<" ";				//wyswietlana jest zawartosc od pierwszego do zawartosc[ROZMIAR-1]
				}
				for(int i=0; i<ostatni; i++){				//i od zawartosc[0] do ostatniego
					cout<<zawartosc[i]<<" ";
				}
			}
		}


		void menu(){
			typ nowa;
			int opcja;
			do{
cout << "1.Dodaj wartosc do kolejki"<<endl;
cout << "2.Usun wartosc z kolejki"<<endl;
cout << "3.Wyswietl kolejke"<<endl;
cout << "0.Koniec"<<endl;
cout << "Wybierz opcje: ";
cin >> opcja;
cout<<endl;
switch(opcja)
{
 case 0: break; 
 case 1: {
 		cout<<"Podaj wartosc ktora chcesz dodac: ";
 		cin>>nowa;
 		 this->Dodaj(nowa);
 		 break;
 	   	 }
 case 2:{
 		if(!this->pusta())
 			cout<<"Zostala usunieta liczba: "<<this->Usun()<<endl;
		else
			cout<<"Blad, lista jest pusta!"<<endl;
		break;
		 }		 
 case 3:{
 		if(!this->pusta())
 			this->Wyswietl();
		else
			cout<<"Brak elementow na liscie!"<<endl;
	break;
 }
}

cout<<endl<<endl;
} while(opcja!=0);
		}			
	private:
		int pierwszy, ostatni;
		typ zawartosc[ROZMIAR];
};


int main(){
		int opcja;
		cout<<"Wybierz typ danych na ktorych chcesz operowac:"<<endl;
		cout<<"1. Liczba calkowita int"<<endl;
		cout<<"2. Typ znakowy char"<<endl;		
		cout<<"3. Liczba zmiennoprzecinkowa float"<<endl;
		cout<<"4. Lancuch znakow string"<<endl;
		cin >> opcja;
		if(opcja==1){
		Kolejka<int> przyklad;
		cout<<endl;
		przyklad.menu();
		}
		if(opcja==2){
			Kolejka<char> przyklad;
		cout<<endl;
		przyklad.menu();
		}
		if(opcja==3){
			Kolejka<float> przyklad;
			cout<<endl;
		przyklad.menu();
		}
		if(opcja==4){
			Kolejka <string> przyklad;
			cout<<endl;
		przyklad.menu();
		}
		else
			cout<<"Bledny parametr!"<<endl;


	return 0;
}


